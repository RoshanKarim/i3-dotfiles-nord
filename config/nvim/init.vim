source $HOME/.config/nvim/vim-plug/plugins.vim

map ; :Files<CR>

let g:lightline = {
      \ 'colorscheme': 'nord',
      \ }

colorscheme nord
