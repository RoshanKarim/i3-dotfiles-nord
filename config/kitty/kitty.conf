# vim:fileencoding=utf-8:ft=conf

# Fully featured
font_family			 Hack Regular Nerd Font Complete Mono
italic_font      Hack Italic Nerd Font Complete Mono
bold_font        Hack Bold Nerd Font Complete Mono
bold_italic_font Hack Bold Italic Nerd Font Complete Mono

# Font size (in pts)
font_size        11.0

# The amount the font size is changed by (in pts) when increasing/decreasing
# the font size in a running terminal.
font_size_delta 2

# Nord theme - ported from https://github.com/arcticicestudio/nord-hyper
foreground            #D8DEE9
background            #2E3440
selection_foreground  #000000
selection_background  #FFFACD
url_color             #0087BD
cursor                #81A1C1

# black
color0   #3B4252
color8   #4C566A

# red
color1   #BF616A
color9   #BF616A

# green
color2   #A3BE8C
color10  #A3BE8C

# yellow
color3   #EBCB8B
color11  #EBCB8B

# blue
color4  #81A1C1
color12 #81A1C1

# magenta
color5   #B48EAD
color13  #B48EAD

# cyan
color6   #88C0D0
color14  #8FBCBB

# white
color7   #E5E9F0
color15  #B48EAD

# Mouse & cursor
cursor_blink_interval     0.4
cursor_stop_blinking_after 4.0
# one of (block, beam, underline)
mouse_hide_wait 3.0

scrollback_lines 10000
scrollback_pager less +G -R
scrollback_in_new_tab no
cursor_shape beam

# Wheel scroll multiplier (modify the amount scrolled by the mouse wheel). Use negative
# numbers to change scroll direction.
wheel_scroll_multiplier 5.0

# The interval between successive clicks to detect double/triple clicks (in seconds)
click_interval 0.5

#Select for double clicking
select_by_word_characters :@-./_~?&=%+#

#don't care, use i3.
focus_follows_mouse yes
remember_window_size   no
enabled_layouts *
initial_window_width   640
initial_window_height  400

# 10 yields ~100 FPS which is more than sufficient for most uses.
repaint_delay    10

# Delay (in milliseconds) before input from the program running in the terminal
# is processed.
input_delay 3

#bell
enable_audio_bell no

# The modifier keys to press when clicking with the mouse on URLs to open the URL
open_url_modifiers ctrl+shift
open_url_with default

# Choose whether to use the system implementation of wcwidth() (used to
# control how many cells a character is rendered in).  If you use the system
# implementation, then kitty and any programs running in it will agree. The
# problem is that system implementations often are based on outdated unicode
# standards and get the width of many characters, such as emoji, wrong. So if
# you are using kitty with programs that have their own up-to-date wcwidth()
# implementation, set this option to no.
use_system_wcwidth yes

# The value of the TERM environment variable to set
term xterm-kitty

#window decorations
window_border_width 1
window_padding_width 0
active_border_color #00ff00
inactive_border_color #cccccc

# Tab-bar colors
active_tab_foreground #000
active_tab_background #eee
inactive_tab_foreground #444
inactive_tab_background #999


# Key mappings

# Clipboard
map ctrl+shift+v        paste_from_clipboard
map ctrl+shift+s        paste_from_selection
map ctrl+shift+c        copy_to_clipboard
map shift+insert        paste_from_selection
map ctrl+shift+o      pass_selection_to_program google-chrome
# map ctrl+shift+o        pass_selection_to_program

# Margin
window_margin_width 8
