# _____  _  __
# |  __ \| |/ /
# | |__) | ' / 
# |  _  /|  <  
# | | \ \| . \ 
# |_|  \_\_|\_\
#

# Enable Powerlevel10k instant prompt.
if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi

# Globals
export ZSH="$HOME/.oh-my-zsh"
ZSH_THEME="powerlevel10k/powerlevel10k"

export EDITOR="/usr/bin/nvim"
export VISUAL="/usr/bin/nvim"
export PATH="$HOME/.local/bin:$PATH"
export PATH="~/.emacs.d/bin:$PATH"
HISTFILE=~/.zhistory
HISTSIZE=1000
SAVEHIST=500
WORDCHARS=${WORDCHARS//\/[&.;]} 

zstyle ':completion:*' matcher-list 'm:{a-zA-Z}={A-Za-z}'      
zstyle ':completion:*' list-colors "${(s.:.)LS_COLORS}"        
zstyle ':completion:*' rehash true             
zstyle ':completion:*' accept-exact '*(N)'
zstyle ':completion:*' use-cache on
zstyle ':completion:*' cache-path ~/.zsh/cache      

plugins=(
    git
    zsh-syntax-highlighting
    zsh-autosuggestions
)

source $ZSH/oh-my-zsh.sh

# Aliases -> navigation
alias cp="rsync --progress"                               
alias df="df -h"   
alias free="free -m"
alias ..="cd .."
alias 2..="cd ../.."
alias 3..="cd ../../.."
alias 4..="cd ../../../.."
alias 5..="cd ../../../../.."

# Aliases -> change `ls` to `exa`
if [ -f /usr/bin/exa ]; then
    alias ls='exa'
    alias la='exa -a'
    alias ll='exa -l'
    alias lla='exa -la'
else
    alias ls='ls --color=auto'
    alias la='ls -a --color=auto'
    alias ll="ls -l --color=auto"
    alias lla="ls -la --color=auto"
fi

# Aliases -> pacman and paru
alias pacs="sudo pacman -S"
alias pacsyu="sudo pacman -Syu"
alias pacr="sudo pacman -Rns"
alias parus="paru -S"
alias paruskip="paru -S --mflags --skipinteg"
alias parur="paru -Rns"
alias unlock="sudo rm /var/lib/pacman/db.lck"
alias cleanup='paru -Rns $(paru -Qtdq)'

# Aliases -> git
alias dotfiles="cd ~/awesome-dotfiles"
alias df-push="git push origin master"
alias df-commit="git add . && git commit -a -m"

# Aliases -> misc
alias ps="ps -efA"
alias yt="youtube-dl"
alias zshrc="nvim ~/.zshrc"
alias apply="source ~/.zshrc"
alias termbin="nc termbin.com 9999"
alias compress="tar -czvf"
alias torrent="transmission-cli"
alias vim="nvim"
alias ss="flameshot gui"
alias vpnon="sudo protonvpn c -f"
alias vpnoff="sudo protonvpn d"

# Easy extractor
ex ()
{
  if [ -f $1 ] ; then
    case $1 in
      *.tar.bz2)   tar xjf $1   ;;
      *.tar.gz)    tar xzf $1   ;;
      *.tar.xz)	   tar xJf $1   ;;
      *.bz2)       bunzip2 $1   ;;
      *.rar)       unrar x $1     ;;
      *.gz)        gunzip $1    ;;
      *.tar)       tar xf $1    ;;
      *.tbz2)      tar xjf $1   ;;
      *.tgz)       tar xzf $1   ;;
      *.zip)       unzip $1     ;;
      *.Z)         uncompress $1;;
      *.7z)        7z x $1      ;;
      *)           echo "'$1' cannot be extracted via ex()" ;;
    esac
  else
    echo "'$1' is not a valid file"
  fi
}

# Setup clean session
clear
bindkey -v

if [ -f /usr/bin/colorscript ]; then
    colorscript -e jangofett
fi

# To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.
[[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh

# dir_colors
test -r ~/.dir_colors && eval $(dircolors ~/.dir_colors)

neofetch
