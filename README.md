<h1>i3 Dotfiles Nord</h1>

These are my dotfiles for i3-gaps, based off of the Nord colorscheme. I've just gotten started learning i3 and this is my first rice, but I think it has come out quite well. Please read below for more information


<h2>Dependencies</h2>

All dependencies are listed as their Arch Linux package name. Additionally, all listed recommended programs are themed.


`nitrogen` - backgrounds

`polybar` - the bar

`picom` - compositor

`rofi` - the menus

`i3-gaps` - self-evident

`lxappearance` - GTK theming

`acpi` (OPTIONAL) - laptops

`pulseaudio` and `pavucontrol` (OPTIONAL) - volume widget

`xdotool` - workspaces script

`jshon` - workspaces script

`dunst` - notifications daemon

`tlp` and `tlpui-git` - power management

`light-locker` - lockscreen


Recommended Programs:


`kitty` - terminal

`firefox` - browser

`brave` - browser for stupid educational software

`nemo` - file manager

`mpd` and `mpc` - music

`element-desktop` - messaging (Discord)

`discord` and `betterdiscordctl` - messaging (Matrix)

`xdg-utils` (QOL) - open links and files directly from applications

`flameshot` - screenshots (working on a widget)


Other:


`zsh` - shell

`ohmyzsh` (NOT AN ARCH PACKAGE) - zsh framework

`powerlevel10k` (NOT AN ARCH PACKAGE) - zsh theme


<h2>Installation</h2>


In order to install the rice, simply clone this repository and run `install`. Make sure you have all required dependencies. If you're on Arch, you can simply run the `archDeps` script to acquire all dependencies and recommended programs. Make sure you have a pacman wrapping AUR helper to make use of this script.

In order to get a wallpaper, simply configure one in `nitrogen`. I've included a few in the `walls` folder. Once you've setup the wallpaper once, i3 will paint it automatically each time you start your computer.

In order to make sure all GTK-based applications are themed, I've included a GTK theme. Please make sure you activate it in `lxappearance`.


<h2>Screenshots</h2>


![desktop picture](media/2020-11-27_22-53.png)


